---
author: Felix W.
date: 25.11.2023
title: Markdown
numbersections: true
---

# Markdown test
This is an easy way for me to see and test out Markdown features.

## Section headings
You can do multilevel section headings by starting a line with the desired number of '#'.
For example:

# This is a level one Section heading. It uses one '#'

## This is a level two heading

### Level 3, you get it

## Blockquotes
You can do blockquotes by starting the line with a '>'.
For example:

> This is a blockquote

## Lists
You can do a list by starting the line with a '+':

+ This is a list element
+ This is another one
+ And here's a third

### Numbered lists
You can also do a numbered list:

1. This is the first item
2. That's the second
3. let's do a third one

## New lines and paragraphs
You start a new line by ending a line with two spaces (␣)  
You start a new paragraph by leaving a blank line.

## Image insertion
You can insert an image using HTML:

![A picture showing how to insert an image](images/image.png)

## Tables
You can also create a table:  

| Header 1 | Header 2 | Header 3 |
| -------- | -------- | -------- |
| Row 1, Col 1 | Row 1, Col 2 | Row 1, Col 3 |
| Row 2, Col 1 | Row 2, Col 2 | Row 2, Col 3 |
| Row 3, Col 1 | Row 3, Col 2 | Row 3, Col 3 |

![A picture showing how to create a table](images/table.png)

## YAML metadata
Now that's the basics of Markdown. It's pretty simple, isn't it? But maybe you want to add an 
author, a title or a date. You can do this by starting the first line of your md file with
3 '-', then you add the metadata, like 'author', 'date', or 'fontsize'. You then end the metadata
section the same way you started it. These are really the most useful, when you convert your md
files to pdf using pandoc. If you want to set your font 
to a bigger size than 12, you also need to include 'documentclass: extarticle' in the metadata
section. 
For further information please refer to to the [official documentation](https://yaml.org/spec/1.2.2/)

## Closing thoughts
I'm certain that this isn't everything Markdown is capable of, but they are the features I need
the most frequently. In the end, I like Markdown because of how easy it is to learn. It took me 
around 15 minutes learning all the stuff in here

![demoman](images/demoman.jpg)
